import numpy as np
import cv2

# Identify pixels above the threshold
# Threshold of RGB > 160 does a nice job of identifying ground pixels only
def color_thresh(img, rgb_thresh=(160, 160, 160)):
    # Create an array of zeros same xy size as img, but single channel
    color_select = np.zeros_like(img[:,:,0])
    # Require that each pixel be above all three threshold values in RGB
    # above_thresh will now contain a boolean array with "True"
    # where threshold was met
    above_thresh = (img[:,:,0] > rgb_thresh[0]) \
                & (img[:,:,1] > rgb_thresh[1]) \
                & (img[:,:,2] > rgb_thresh[2])
    # Index the array of zeros with the boolean array and set to 1
    color_select[above_thresh] = 1
    # Return the binary image
    return color_select

# Define a function to convert from image coords to rover coords
def rover_coords(binary_img):
    # Identify nonzero pixels
    ypos, xpos = binary_img.nonzero()
    # Calculate pixel positions with reference to the rover position being at the 
    # center bottom of the image.  
    x_pixel = -(ypos - binary_img.shape[0]).astype(np.float)
    y_pixel = -(xpos - binary_img.shape[1]/2 ).astype(np.float)
    return x_pixel, y_pixel


# Define a function to convert to radial coords in rover space
def to_polar_coords(x_pixel, y_pixel):
    # Convert (x_pixel, y_pixel) to (distance, angle) 
    # in polar coordinates in rover space
    # Calculate distance to each pixel
    dist = np.sqrt(x_pixel**2 + y_pixel**2)
    # Calculate angle away from vertical for each pixel
    angles = np.arctan2(y_pixel, x_pixel)
    return dist, angles

# Define a function to map rover space pixels to world space
def rotate_pix(xpix, ypix, yaw):
    # Convert yaw to radians
    yaw_rad = yaw * np.pi / 180
    xpix_rotated = (xpix * np.cos(yaw_rad)) - (ypix * np.sin(yaw_rad))
                            
    ypix_rotated = (xpix * np.sin(yaw_rad)) + (ypix * np.cos(yaw_rad))
    # Return the result  
    return xpix_rotated, ypix_rotated

def translate_pix(xpix_rot, ypix_rot, xpos, ypos, scale): 
    # Apply a scaling and a translation
    xpix_translated = (xpix_rot / scale) + xpos
    ypix_translated = (ypix_rot / scale) + ypos
    # Return the result  
    return xpix_translated, ypix_translated


# Define a function to apply rotation and translation (and clipping)
# Once you define the two functions above this function should work
def pix_to_world(xpix, ypix, xpos, ypos, yaw, world_size, scale):
    # Apply rotation
    xpix_rot, ypix_rot = rotate_pix(xpix, ypix, yaw)
    # Apply translation
    xpix_tran, ypix_tran = translate_pix(xpix_rot, ypix_rot, xpos, ypos, scale)
    # Perform rotation, translation and clipping all at once
    x_pix_world = np.clip(np.int_(xpix_tran), 0, world_size - 1)
    y_pix_world = np.clip(np.int_(ypix_tran), 0, world_size - 1)
    # Return the result
    return x_pix_world, y_pix_world

# Define a function to perform a perspective transform
def perspect_transform(img, src, dst):
           
    M = cv2.getPerspectiveTransform(src, dst)
    warped = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))# keep same size as input image
    
    return warped


# Apply the above functions in succession and update the Rover state accordingly
def perception_step(Rover):
    # Perform perception steps to update Rover()
    # TODO: 
    # NOTE: camera image is coming to you in Rover.img
    # 1) Define source and destination points for perspective transform
    image = Rover.img.copy()
#     source = np.float32([[119, 96], 
#                  [200,96], 
#                  [304,139], 
#                  [16,139]]) #My measured values from Perception exercise.
#     destination = np.float32([[image.shape[1]//2-10, image.shape[0]-10], 
#                  [image.shape[1]//2, image.shape[0]-10], 
#                  [image.shape[1]//2, image.shape[0]], 
#                  [image.shape[1]//2-10, image.shape[0]]]) #10px*10px Bottom center
    
    ###From test notebook:
    # Define calibration box in source (actual) and destination (desired) coordinates
    # These source and destination points are defined to warp the image
    # to a grid where each 10x10 pixel square represents 1 square meter
    # The destination box will be 2*dst_size on each side
    dst_size = 5 
    # Set a bottom offset to account for the fact that the bottom of the image 
    # is not the position of the rover but a bit in front of it
    # this is just a rough guess, feel free to change it!
    bottom_offset = 6
    source = np.float32([[14, 140], [301 ,140],[200, 96], [118, 96]])
    destination = np.float32([[image.shape[1]/2 - dst_size, image.shape[0] - bottom_offset],
                  [image.shape[1]/2 + dst_size, image.shape[0] - bottom_offset],
                  [image.shape[1]/2 + dst_size, image.shape[0] - 2*dst_size - bottom_offset], 
                  [image.shape[1]/2 - dst_size, image.shape[0] - 2*dst_size - bottom_offset],
                  ])
    # 2) Apply perspective transform
    transformed_image = perspect_transform(image, source, destination)
    transformed_image_background_ind = np.argwhere(transformed_image[:,:,0]==0)
    # 3) Apply color threshold to identify navigable terrain/obstacles/rock samples
    #Find terrain:
    navigable = color_thresh(transformed_image, rgb_thresh=(160, 160, 160)) #Better fidelity with 170 instead of 160. 150 makes it worse.
    #Invert to find obstacles and substract background from perspective transform:
    obstacle = np.invert(navigable)
    obstacle = obstacle - np.min(obstacle) #normalize after inverting
    obstacle[transformed_image_background_ind[:,0],transformed_image_background_ind[:,1]] = 0
    #Find rock samples by thresholding to find yellow (RGB)(255,255,0)
    rock_thresh=(110, 100, 90) #Using a color picker tool, found the RBG color of some rocks (150, 140, 80), which fall within these bounds, then lowering them further because rover doesn't think the rocks are rocks.
    rock = np.zeros_like(image[:,:,0])
    above_thresh = (image[:,:,0] > rock_thresh[0]) \
                & (image[:,:,1] > rock_thresh[1]) \
                & (image[:,:,2] < rock_thresh[2]) #Referenced from function above but changed it up for the  in Blue channel
    rock[above_thresh] = 1
    
    # 4) Update Rover.vision_image (this will be displayed on left side of screen)
        # Example: Rover.vision_image[:,:,0] = obstacle color-thresholded binary image
        #          Rover.vision_image[:,:,1] = rock_sample color-thresholded binary image
        #          Rover.vision_image[:,:,2] = navigable terrain color-thresholded binary image
       
    Rover.vision_image[:,:,0] = obstacle * 255 #Multiple by 255 for uint8 colors
    Rover.vision_image[:,:,1] = rock* 255
    Rover.vision_image[:,:,2] = navigable* 255

    # 5) Convert map image pixel values to rover-centric coords
    navigable_x_rover, navigable_y_rover =  rover_coords(navigable)
    obstacle_x_rover, obstacle_y_rover =  rover_coords(obstacle)
    rock_x_rover, rock_y_rover =  rover_coords(rock)
    
    # 6) Convert rover-centric pixel values to world coordinates
    navigable_x_world,navigable_y_world =  pix_to_world(navigable_x_rover, navigable_y_rover, Rover.pos[0], Rover.pos[1], Rover.yaw, Rover.worldmap.shape[0], 10)
    obstacle_x_world ,obstacle_y_world=  pix_to_world(obstacle_x_rover, obstacle_y_rover, Rover.pos[0], Rover.pos[1], Rover.yaw, Rover.worldmap.shape[0],  10)
    rock_x_world,rock_y_world =  pix_to_world(rock_x_rover, rock_y_rover, Rover.pos[0], Rover.pos[1], Rover.yaw, Rover.worldmap.shape[0], 10)
    #6.5) Save old navigable map to create bigger weight for steering towards uncharted domains
    old_navigable_world = Rover.worldmap[:,:,2].copy()
    
    # 7) Update Rover worldmap (to be displayed on right side of screen)
        # Example: Rover.worldmap[obstacle_y_world, obstacle_x_world, 0] += 1
        #          Rover.worldmap[rock_y_world, rock_x_world, 1] += 1
        #          Rover.worldmap[navigable_y_world, navigable_x_world, 2] += 1
#     if Rover.mode == 'stop': #Don't apply map if we're stopping
    pitch_threshold = 0.1
    if Rover.pitch<pitch_threshold or np.abs(Rover.pitch-360)<pitch_threshold:
        Rover.worldmap[obstacle_y_world, obstacle_x_world, 0] += 1
        Rover.worldmap[rock_y_world, rock_x_world, 1] += 1
        Rover.worldmap[navigable_y_world, navigable_x_world, 2] += 2
    
    #7.5) Weight steering towards uncharted map:
    #Because of clipping and partially because of scaling, we can't really invert the world map back into a rover map. It would also take up a lot of memory to save an unclipped and unscaled map, so we're going to favor angles.
    
    rover_centric_pixel_distances,rover_centric_angles= to_polar_coords(navigable_x_rover, navigable_y_rover)
    pixels_being_seen = np.argwhere(Rover.worldmap[navigable_y_world, navigable_x_world, 2].ravel()>0)
    if len(pixels_being_seen)>0:
        pixels_revisited = np.argwhere(Rover.worldmap[navigable_y_world, navigable_x_world, 2].ravel()>5)
        if (len(pixels_revisited)/len(pixels_being_seen))>=1:
            raveled_map =Rover.worldmap[navigable_y_world, navigable_x_world, 2].ravel()
            print("A lot of overlap between old map and current view, current mean:",np.mean(rover_centric_angles))
            if (Rover.vel>1.8 and len(Rover.nav_angles) > Rover.go_forward): #To avoid getting stuck at a wall
                rover_centric_angles[pixels_revisited] = rover_centric_angles[pixels_revisited]* (1/(raveled_map[pixels_revisited]**4))
                print("Weighing new map data angles more than already explored data")
            Rover.worldmap[Rover.worldmap[:, :, 2]>1, 2] -= 0.01 #Decrease penalty over time 
        else:
            print("no overlap yet: ",len(pixels_revisited)/len(pixels_being_seen))
#         weight_map = np.zeros_like(old_navigable_world)
#         weight_map[navigable_y_world,navigable_x_world] = 1 #make current pixels white
#         #Draw a bounding box around the world map's ROI
#         box = [np.min(navigable_x_world),np.max(navigable_x_world),np.min(navigable_y_world),np.max(navigable_y_world)] 
#         pixels_in_current_map = (np.argwhere(weight_map[box[2]:box[3],box[0]:box[1]].ravel() == 1)) #count the overlap
#         pixels_in_world_map = (np.argwhere( old_navigable_world[box[2]:box[3],box[0]:box[1]].ravel()>0)) #count the pixels
#         num_pixel_in_current_map = len(pixels_in_current_map) #count the overlap
#         num_pixels_in_world_map = len(pixels_in_world_map) #count the pixels
#         if (num_pixels_in_world_map)>0:
#             print("Overlap:",num_pixel_in_current_map,num_pixels_in_world_map,
#                   num_pixel_in_current_map/num_pixels_in_world_map)

#             if ( num_pixel_in_current_map/num_pixels_in_world_map>0.8): #if 80% matching, which we should do something to favor uncharted maps
#                 print("A lot of overlap between old map and current view, current mean:",np.mean(rover_centric_angles))
#                 indecies = np.unique([pixels_in_current_map])
#                 rover_centric_angles[indecies] = 0
#                 print("Mean after weighing:",np.mean(rover_centric_angles), "indecies used:",len(indecies))

      
        # 8) Convert rover-centric pixel positions to polar coordinates
        # Update Rover pixel distances and angles
        Rover.nav_dists = rover_centric_pixel_distances
        Rover.nav_angles = rover_centric_angles
        
    
    return Rover