import numpy as np


# This is where you can build a decision tree for determining throttle, brake and steer 
# commands based on the output of the perception_step() function
def decision_step(Rover):

    # Implement conditionals to decide what to do given perception data
    # Here you're all set up with some basic functionality but you'll need to
    # improve on this decision tree to do a good job of navigating autonomously!
    
    #Save last few x,y positions to make sure we're not stuck:
    min_stuck_num = 120
    stuck_tolerance = 0.05
    if Rover.mode != 'stop':
        Rover.last_pos = np.append(Rover.last_pos,[(Rover.pos[0],Rover.pos[1])],axis=0)
    if len(Rover.last_pos)>min_stuck_num:
        Rover.last_pos = Rover.last_pos[-min_stuck_num-1:,:] #keep only last 30 to save memory
        print("stuck check", np.abs(np.mean(Rover.last_pos,axis=0)-Rover.pos),
              np.abs(np.mean(Rover.last_pos,axis=0)-Rover.pos)<stuck_tolerance,
              np.all(np.abs(np.mean(Rover.last_pos,axis=0)-Rover.pos)<stuck_tolerance),
              "Mode:",Rover.mode)
    # Example:
    # Check if we have vision data to make decisions with
    if Rover.nav_angles is not None:
        #Stop if we're near a rock to pick it up
        if Rover.near_sample and Rover.vel != 0:
            # Set mode to "stop" and hit the brakes!
            Rover.throttle = 0
            # Set brake to stored brake value
            Rover.brake = Rover.brake_set
            Rover.steer = 0
            Rover.mode = 'stop'
        # Check for Rover.mode status
        elif Rover.mode == 'forward': 
            # Check the extent of navigable terrain
            if len(Rover.nav_angles) >= Rover.stop_forward:  
                # If mode is forward, navigable terrain looks good 
                # and velocity is below max, then throttle 
                if Rover.vel < Rover.max_vel:
                    # Set throttle value to throttle setting
                    Rover.throttle = Rover.throttle_set
                else: # Else coast
                    Rover.throttle = 0
                Rover.brake = 0
                # Set steering to average angle clipped to the range +/- 15
                angle = np.clip(np.median(Rover.nav_angles * 180/np.pi), -15, 15)
                Rover.steer = angle
            # If there's a lack of navigable terrain pixels then go to 'stop' mode
            elif len(Rover.nav_angles) < Rover.stop_forward:
                    # Set mode to "stop" and hit the brakes!
                    Rover.throttle = 0
                    # Set brake to stored brake value
                    Rover.brake = Rover.brake_set
                    Rover.steer = 0
                    Rover.mode = 'stop'
            
            
        # If we're already in "stop" mode then make different decisions
        elif Rover.mode == 'stop':
            # If we're in stop mode but still moving keep braking
            if Rover.vel > 0.2:
                Rover.throttle = 0
                Rover.brake = Rover.brake_set
                Rover.steer = 0
            # If we're not moving (vel < 0.2) then do something else
            elif Rover.vel <= 0.2:
                # Now we're stopped and we have vision data to see if there's a path forward
                if len(Rover.nav_angles) < Rover.go_forward:
#                     Rover.throttle = -0.2
                    Rover.throttle = 0
                    # Release the brake to allow turning
                    Rover.brake = 0
                    # Turn range is +/- 15 degrees, when stopped the next line will induce 4-wheel turning
                    if np.abs(Rover.steer) != 15: #Stops it from getting stuck between -ve and +ve 15, this is easier than saving previous state
                        if Rover.steer!=0:
                            Rover.steer = -15 * (Rover.steer/np.abs(Rover.steer)) # Could be more clever here about which way to turn
                        else:
                            Rover.steer = -15 # Could be more clever here about which way to turn
                # If we're stopped but see sufficient navigable terrain in front then go!
                if len(Rover.nav_angles) >= Rover.go_forward:
                    # Set throttle back to stored value
                    Rover.throttle = Rover.throttle_set
                    # Release the brake
                    Rover.brake = 0
                    # Set steer to mean angle
                    Rover.steer = np.clip(np.median(Rover.nav_angles * 180/np.pi), -15, 15)
                    Rover.mode = 'forward'
    # Just to make the rover do something 
    # even if no modifications have been made to the code
    else:
        Rover.throttle = Rover.throttle_set
        Rover.steer = 0
        Rover.brake = 0
    if len(Rover.last_pos)>=min_stuck_num and np.all(np.abs(np.mean(Rover.last_pos,axis=0)-Rover.pos)<stuck_tolerance) and Rover.vel<=0.2: #We may be stuck
            print("We're stuck, trying to reverse to get out:")
            # Set mode to "stop" and turn around
            Rover.throttle = -15.0
            # Set brake to stored brake value
            Rover.brake = 0
            Rover.steer = -15.0
                    
    # If in a state where want to pickup a rock send pickup command
    if Rover.near_sample and Rover.vel == 0 and not Rover.picking_up:
        Rover.send_pickup = True
    
    return Rover

